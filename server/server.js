import express from 'express';
import formData from 'express-form-data';
import 'express-async-errors';
import cookieParser from 'cookie-parser';
import debug from 'debug';
import logger from 'morgan';
import cors from 'cors';
import os from 'os';
import { config } from 'dotenv';
import path from 'path';
import Routes from './routes/v1';

config();
const log = debug('dev');
const app = express();

const options = {
  uploadDir: os.tmpdir(),
  autoClean: true,
};

app.use(formData.parse(options));
app.use(formData.format());
app.use(formData.stream());
app.use(formData.union());
// app.use(express.urlencoded({ extended: true }));
// app.use(express.json());

const whitelist = process.env.whiteList.split(',');
const corsOptions = {
  origin(origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
  credentials: true,
  allowedHeaders: [
    'Access-Control-Allow-Origin',
    'Access-Control-Allow-Headers',
    'Content-Type',
  ],
};

app.use(cors(corsOptions));
app.use(cookieParser());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, '../site/build')));

app.get('/api/v1', (req, res) =>
  res.status(200).json({
    message: 'Btn Upload API',
  })
);

// Routes(app);
app.use('/api/v1', Routes);

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../site/build', 'index.html'));
});

// to catch a 404 and send to error handler
app.use((req, res) =>
  res.status(404).json({
    status: 404,
    error: `Route ${req.url} Not found`,
  })
);

// app error handler, to handle sync and asyc errors
app.use((err, req, res, next) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  if (res.headersSent) return next(err);

  log(err);
  return res.status(err.status || 500).json({
    status: res.statusCode,
    error: err.message,
  });
});

export default app;
