import { errorStat, successStat } from './Utilities';
export default {
  errorStat,
  successStat,
};
